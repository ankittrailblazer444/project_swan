<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gpttable extends Model
{
    use HasFactory;
    protected $fillable = [
        'gptkey','gpttitle','gptdesc'
    ];
}
