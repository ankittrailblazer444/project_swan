<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\gpttable;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PythonController extends Controller
{
    public function PythonScript()
    {
        // $process = new Process("python3 /var/www/laravel/Laravel_Python_Example/app/PythonScript/test.py");
        // $process = new Process(['C:/Users/ad/AppData/Local/Microsoft/WindowsApps/python3.10.exe',
        // 'C:/laragon/www/project_swan/resources/views/python-file/hello.py']);

        $process = new Process(['C:/laragon/bin/python/python-3.10/python.exe',
        'C:/laragon/www/project_swan/resources/views/python-file/gpt.py']);
        $process->setTimeout(300);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return back()->with('success','New Blog added successfully!',[view('blog-gpt')]);
        $data = $process->getOutput();    
        dd($data);

        // return back()->with('success','New Blog added successfully!',[view('blog-gpt')]);

    } 
}
