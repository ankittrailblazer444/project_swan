<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\table;
use Illuminate\Http\Request;
use DB;
class TableController extends Controller
{
    public function tableindex()
    {   
        $blogdata=table::select('title','key','value','image','created_at','author')->get();
        $title = table::select('title')->get();
        $key = table::select('key')->get();
        $value = table::select('value')->get();
        $image = table::select('image')->get();
        $blogdate = table::select('created_at')->get();
        $author = table::select('author')->get();
        // return $title;
        return view('blog',[
            'title'=>$title,
            'blogno'=>$key,
            'desc'=>$value,
            'blogimage'=>$image,
            'blogdate'=>$blogdate,
            'author'=>$author
        ]);
        // return view('index');
    }

    public function showform()
    {
        return view('blog-create');
    }

    public function showeditform()
    {
        return view('blog-edit');
    }


    public function save(Request $request)
    {
        $destinationPath='assets\images\blog-post';
        $myimage = $request->file->getClientOriginalName();
        $request->file->move(public_path($destinationPath), $myimage);
        $data = [
            'title'=> $request->title,
            'key'=> $request->blogno,
            'value'=> $request->desc,
            'image'=> $destinationPath.'\\'.$request->file->getClientOriginalName(),
            'author'=> $request->author
            // 'image'=> $request->file->getClientOriginalName()
        ];
        // return back()  
        $result = table::create($data);
        // echo public_path($destinationPath);
        // if($result){
        //     return "Save";
        // }
        // else{
        //     return "error!!";
        // }
        // echo "Record updated successfully;
        return back()->with('success','Post created successfully!',[view('blog-create')]);;


    }

    public function destroy($id) {
        $deleted = DB::table('tables')->where('key',$id)->delete();
        if($deleted)
        // DB::delete('delete from tables where `key` = ?',[$id]);
        return back()->with('warning','Post deleted successfully!',[view('table')]);
        return back()->with('warning','Something went wrong!',[view('table')]);
        // echo "Record deleted successfully.<br/>";
        // echo '<a href = "/delete-records">Click Here</a> to go back.';
     }

     
     public function edit(Request $request)
    {
        $destinationPath='assets\images\blog-post';
        $myimage = $request->file->getClientOriginalName();
        $request->file->move(public_path($destinationPath), $myimage);
        $id = $request->blogno;
        $values = [
            'title'=> $request->title,
            'key'=> $request->blogno,
            'value'=> $request->desc,
            'image'=> $destinationPath.'\\'.$request->file->getClientOriginalName(),
            'author'=> $request->author
            // 'image'=> $request->file->getClientOriginalName()
        ];
        // return back()  
        $updated = DB::table('tables')->where('key',$id)->update($values);
        // echo public_path($destinationPath);
        // if($result){
        //     return "Save";
        // }
        // else{
        //     return "error!!";
        // }
        // echo "Record updated successfully;
        return back()->with('success','Post Updated successfully!',[view('table')]);


    }

}
