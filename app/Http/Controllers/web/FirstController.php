<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\setting;
use Illuminate\Http\Request;

class FirstController extends Controller
{
    public function index()
    {
        $phone=setting::where('key','phone')->first();
        $email=setting::where('key','email')->first();
        $address=setting::where('key','address')->first();
        $logo=setting::where('key','logo')->first();
        return view('index',[
            'phone'=>$phone,
            'email'=>$email,
            'address'=>$address,
            'logo'=>$logo
        ]);
        // return view('index');
    }
}
