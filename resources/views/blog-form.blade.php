@php
$title = App\Models\table::select('title')->get();
$key = App\Models\table::select('key')->get();
$value = App\Models\table::select('value')->get();
$image = App\Models\table::select('image')->get();
$blogdate = App\Models\table::select('created_at')->get();
$author = App\Models\table::select('author')->get();
$i = $_GET['id'];
@endphp
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="title">Title</label>
        <input type="text" name ="title" class="form-control" required id="title" 
        value = "{{!empty($title[$i]->title) ? $title[$i]->title : ''}}">
      </div>
    </div>
    <div class="form-group">
      <label for="desc">Blog description</label>
      <input type="text" name ="desc" class="form-control w-75  p-3 " id="desc" required placeholder="Desc here"
      value = "{{!empty($value[$i]->value) ? $value[$i]->value : ''}}">
    </div>
    <div class="form-group col-md-2">
        <label for="blogno">Blog Number</label>
        <select id="blogno" name ="blogno" class="form-control" required>
          <option selected>{{$i+1}}</option>
          <!-- <option></option> -->
          <!-- <option>Delhi</option> -->
        </select>
      </div>
    <!-- <div class="form-row">
      <div class="form-group col-md-1">
        <label for="blogno">Blog No</label>
        <input type="number" name ="blogno" class="form-control" min ={{count($title)+1}} max = {{count($title)+1}} required id="blogno">
      </div> -->
      <div class="form-group">
      <label for="author">Blog Author Name</label>
      <input type="text" name ="author" class="form-control col-md-4 " id="author" required placeholder="Enter Author Name"
      value = "{{!empty($author[$i]->author) ? $author[$i]->author : ''}}">
      </div>
      <div class="form-group col-md-4">
        <label for="file"><img src="{{!empty($image[$i]->image) ? $image[$i]->image : ''}}" alt="" class="rounded-circle avatar avatar-50 mr-2">
        Blog File</label>
        <input type="file" name ="file"  class="form-control" required id="file"
        value = "{{!empty($image[$i]->image) ? $image[$i]->image : ''}}">
      </div>
    <button type="submit" class="btn btn-primary">Submit Here</button>
    <button type="reset" value="Reset" class="btn btn-primary">Clear</button>
    </div>
    </div>