@php
$title = App\Models\table::select('title')->get();
$key = App\Models\table::select('key')->get();
$value = App\Models\table::select('value')->get();
$image = App\Models\table::select('image')->get();
$blogdate = App\Models\table::select('created_at')->get();
$author = App\Models\table::select('author')->get();
@endphp
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">

    <!-- favicons -->
    <link rel="apple-touch-icon" href="{{asset('assets\tableassets\img\favicon-apple.png')}}">
    <link rel="icon" href="{{asset('assets\tableassets\img\favicon.png')}}">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css')}}"> -->

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\materializeicon\material-icons.css')}}">

    <!-- aniamte CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\animatecss\animate.css')}}">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\swiper\css\swiper.min.css')}}">

    <!-- daterange CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\daterangepicker.css')}}">

    <!-- dataTable CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\DataTables-1.10.18\css\dataTables.bootstrap4.min.css')}}">

    <!-- jvector map CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap-2.0.3.css')}}">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="{{asset('assets\tableassets\css\purplesidebar.css')}}" type="text/css">

    <title>GoTRI</title>
</head>

<body class="fixed-header sidebar-right-close">
    <div class="wrapper">
    
        @include('admin-left-sidebar')
        @include('admin-header')
        @include('flash-message')
        <div class="container-fluid bg-light-opac">
            <div class="row">
                <div class="container my-3 main-container">
                    <div class="row align-items-center">
                        <div class="col">
                            <h2 class="content-color-primary page-title">DataTable</h2>
                            <p class="content-color-secondary page-sub-title">Creative, amazing, awesome and unique</p>
                        </div>
                        <div class="col-auto">
                            <a href = "blog-create?id={{count($title)}}" class="btn btn-rounded pink-gradient text-uppercase pr-3"><i class="material-icons">add</i> <span class="text-hide-xs">
                                Create Blog</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- content page -->
        <div class="container-fluid mt-4 main-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card mb-4 fullscreen">
                        <div class="card-header">
                            <div class="media">
                                <div class="media-body">
                                    <h4 class="content-color-primary mb-0">Blog Data</h4>
                                </div>
                                <a href="javascript:void(0);" class="icon-circle icon-30 content-color-secondary fullscreenbtn">
                                    <i class="material-icons ">crop_free</i>
                                </a>
                            </div>
                        </div>
                        
                        <div class="card-body">
                            <table class="table w-100 " id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Blog No</th>
                                        <th>Blog Desc</th>
                                        <th>Author</th>
                                        <th>Published date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=0; $i<count($title); $i++)
                                    <tr class="odd">
                                        <td><a href="blog-details?id={{$i}}"><img src="{{$image[$i]->image}}" alt="" class="rounded-circle avatar avatar-50 mr-2">{{Str::limit($title[$i]->title,20)}}</a>
                                        </td>
                                        <td>{{$key[$i]->key}}</td>
                                        <td>{{Str::limit($value[$i]->value,20)}}</td>
                                        <td class="center">{{$author[$i]->author}}</td>
                                        <td class="center"><span>{{$blogdate[$i]->created_at}}</span></td>
                                        <td class="center"><span class ="btn btn-sm btn-outline-success">Active</span></td>
                                        <td class="center">
                                            <!-- <a href="" class=" btn btn-sm btn-outline-success">Active</a> -->
                                            <a href="blog-edit?id={{$i}}" class="btn btn-sm btn-outline-warning">Update</a>
                                            <a href="delete/{{$key[$i]->key}}" class="btn btn-sm btn-outline-danger">Delete</a>
                                            <!-- <a href="" class="btn btn-sm btn-outline-success"></a> -->
                                        </td>
                                    </tr>
                                    @endfor
                                    <!-- <tr class="even ">
                                        <td><img src="img\user2.png" alt="" class="rounded-circle avatar avatar-50 mr-2">Alone Guy
                                        </td>
                                        <td>infoatmaxartkiller.in</td>
                                        <td>+91 000 000 0000</td>
                                        <td class="center">18</td>
                                        <td class="center"><span class="btn btn-outline-success btn-sm">Active</span></td>
                                        <td class="center">
                                            <a href="" class=" btn btn-link btn-sm "><img class="border-0 vm" src="img\facebook.png" alt=""></a>
                                            <a href="" class="btn btn-link btn-sm"><img class="border-0 vm" src="img\pinterest.png" alt=""></a>
                                            <a href="" class="btn btn-link btn-sm"><img class="border-0 vm" src="img\twitter.png" alt=""></a>
                                            <a href="" class="btn btn-link btn-sm"><img class="border-0 vm" src="img\linkedin.png" alt=""></a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
        <!-- main container ends -->

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('assets\tableassets\js\jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\js\popper.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\bootstrap-4.1.3\js\bootstrap.min.js')}}"></script>

    <!-- Cookie jquery file -->
    <script src="{{asset('assets\tableassets\vendor\cookie\jquery.cookie.js')}}"></script>

    <!-- sparklines chart jquery file -->
    <script src="{{asset('assets\tableassets\vendor\sparklines\jquery.sparkline.min.js')}}"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="{{asset('assets\tableassets\vendor\circle-progress\circle-progress.min.js')}}"></script>

    <!-- Swiper carousel jquery file -->
    <script src="{{asset('assets\tableassets\vendor\swiper\js\swiper.min.js')}}"></script>

    <!-- Chart js jquery file -->
    <script src="{{asset('assets\tableassets\vendor\chartjs\Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\chartjs\utils.js')}}"></script>

    <!-- DataTable jquery file -->
    <script src="{{asset('assets\tableassets\vendor\DataTables-1.10.18\js\jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\DataTables-1.10.18\js\dataTables.bootstrap4.min.js')}}"></script>

    <!-- datepicker jquery file -->
    <script src="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\moment.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\daterangepicker.js')}}"></script>

    <!-- jVector map jquery file -->
    <script src="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- circular progress file -->
    <script src="{{asset('assets\tableassets\vendor\circle-progress\circle-progress.min.js')}}"></script>

    <!-- Application main common jquery file -->
    <script src="{{asset('assets\tableassets\js\main.js')}}"></script>

    <!-- page specific script -->
    <script>
        "use strict"
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                "order": [
                    [3, "desc"]
                ]
            });
        });

    </script>
</body>

</html>
