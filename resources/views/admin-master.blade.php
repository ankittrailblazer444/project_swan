@php
$title = App\Models\table::select('title')->get();
$key = App\Models\table::select('key')->get();
$value = App\Models\table::select('value')->get();
$image = App\Models\table::select('image')->get();
$blogdate = App\Models\table::select('created_at')->get();
$author = App\Models\table::select('author')->get();
@endphp
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">

    <!-- favicons -->
    <link rel="apple-touch-icon" href="{{asset('assets\tableassets\img\favicon-apple.png')}}">
    <link rel="icon" href="{{asset('assets\tableassets\img\favicon.png')}}">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css')}}"> -->

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\materializeicon\material-icons.css')}}">

    <!-- aniamte CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\animatecss\animate.css')}}">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\swiper\css\swiper.min.css')}}">

    <!-- daterange CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\daterangepicker.css')}}">

    <!-- dataTable CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\DataTables-1.10.18\css\dataTables.bootstrap4.min.css')}}">

    <!-- jvector map CSS -->
    <link rel="stylesheet" href="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap-2.0.3.css')}}">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="{{asset('assets\tableassets\css\purplesidebar.css')}}" type="text/css">
    
    <title>GoTRI</title>
</head>

<body class="fixed-header sidebar-right-close">
    <div class="wrapper">
    
        @include('admin-left-sidebar')
        @include('admin-header')
            <!-- content page -->
        <content>
            @yield('content')
        </content>

    </div>
        <!-- main container ends -->

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('assets\tableassets\js\jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\js\popper.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\bootstrap-4.1.3\js\bootstrap.min.js')}}"></script>

    <!-- Cookie jquery file -->
    <script src="{{asset('assets\tableassets\vendor\cookie\jquery.cookie.js')}}"></script>

    <!-- sparklines chart jquery file -->
    <script src="{{asset('assets\tableassets\vendor\sparklines\jquery.sparkline.min.js')}}"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="{{asset('assets\tableassets\vendor\circle-progress\circle-progress.min.js')}}"></script>

    <!-- Swiper carousel jquery file -->
    <script src="{{asset('assets\tableassets\vendor\swiper\js\swiper.min.js')}}"></script>

    <!-- Chart js jquery file -->
    <script src="{{asset('assets\tableassets\vendor\chartjs\Chart.bundle.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\chartjs\utils.js')}}"></script>

    <!-- DataTable jquery file -->
    <script src="{{asset('assets\tableassets\vendor\DataTables-1.10.18\js\jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\DataTables-1.10.18\js\dataTables.bootstrap4.min.js')}}"></script>

    <!-- datepicker jquery file -->
    <script src="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\moment.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\bootstrap-daterangepicker-master\daterangepicker.js')}}"></script>

    <!-- jVector map jquery file -->
    <script src="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap.js')}}"></script>
    <script src="{{asset('assets\tableassets\vendor\jquery-jvectormap\jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- circular progress file -->
    <script src="{{asset('assets\tableassets\vendor\circle-progress\circle-progress.min.js')}}"></script>

    <!-- Application main common jquery file -->
    <script src="{{asset('assets\tableassets\js\main.js')}}"></script>

    <!-- page specific script -->
    <script>
        "use strict"
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                "order": [
                    [3, "desc"]
                ]
            });
        });

    </script>
</body>

</html>
