<?php

use App\Http\Controllers\web\FirstController;
use App\Http\Controllers\web\TableController;
use App\Http\Controllers\web\PythonController;
use App\Http\Controllers\web\GptController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('home',[FirstController::class,'index']);
Route::get('index',[FirstController::class,'index']);
Route::get('/',[FirstController::class,'index']);
Route::post('save',[TableController::class,'save'])->name('save_user');
Route::post('savegpt',[GptController::class,'savegpt'])->name('save_gpt');
Route::get('blog-create',[TableController::class,'showform']);
Route::view('blog-gpt','blog-gpt');
Route::get('blog-edit',[TableController::class,'showeditform']);
Route::post('edit',[TableController::class,'edit'])->name('update_blog');
Route::get('table',[TableController::class,'tableindex']);
Route::get('blog',[TableController::class,'tableindex']);
Route::get('delete/{key}',[TableController::class,'destroy']);
// Route::view('home','index');
Route::view('category','category');
Route::view('404_2','404_2');
Route::view('404_3','404_3');
// Route::view('form','form');
Route::view('blog-details','blog-details');
// Route::view('blog','blog');
Route::view('category','category');
Route::view('checkout','checkout');
Route::view('contact-us','contact-us');
Route::view('contact','contact');
Route::view('detail','detail');
Route::view('faq','faq');
Route::view('index6c11','index6c11');
Route::view('index8a95','index8a95');
Route::view('my-wishlist','my-wishlist');
Route::view('product-comparison','product-comparison');
Route::view('shopping-cart','shopping-cart');
Route::view('sign-in','sign-in');
Route::view('terms-conditions','terms-conditions');
Route::view('track-orders','track-orders');
Route::view('admin','adminpage/admin');
Route::view('admin/blog','adminpage/blog');
Route::view('table','table');
Route::view('check','check-code');
Route::get('python',[PythonController::class,'PythonScript'])->name('blog-gpt');

